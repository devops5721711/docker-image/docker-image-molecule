FROM docker:24.0.5-cli

WORKDIR /role

ENV ANSIBLE_FORCE_COLOR="true"

RUN apk add --no-cache \
    python3 python3-dev py3-pip gcc git curl build-base \
    autoconf automake py3-cryptography linux-headers \
    musl-dev libffi-dev openssl-dev openssh

RUN pip --no-cache-dir install \
    ansible \
    ansible-lint \
    molecule \
    molecule-plugins[docker] \
    molecule-plugins[vagrant] 

ENTRYPOINT ["/bin/sh", "-c"]